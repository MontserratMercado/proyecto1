//
//  main.m
//  Proyecto1
//
//  Created by Montserrat Mercado on 30/07/18.
//  Copyright © 2018 Montserrat Mercado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  AppDelegate.h
//  Proyecto1
//
//  Created by Montserrat Mercado on 30/07/18.
//  Copyright © 2018 Montserrat Mercado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

